program RedisClient;

uses
  {$IFDEF madExcept}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  {$ENDIF}
  Vcl.Forms,
  uMain in 'uMain.pas' {frmMain},
  Redis.Connection in 'Redis.Connection.pas',
  Redis in 'Redis.pas',
  Redis.TCPClient in 'Redis.TCPClient.pas',
  uTerminalFrm in 'uTerminalFrm.pas' {frmTerminal};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
