object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'RedisClient'
  ClientHeight = 101
  ClientWidth = 175
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 12
    Top = 11
    Width = 22
    Height = 13
    Caption = 'Host'
  end
  object Label2: TLabel
    Left = 14
    Top = 38
    Width = 20
    Height = 13
    Caption = 'Port'
  end
  object edHost: TEdit
    Left = 40
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'localhost'
  end
  object edPort: TEdit
    Left = 40
    Top = 35
    Width = 121
    Height = 21
    TabOrder = 1
    Text = '6379'
  end
  object btOpen: TButton
    Left = 40
    Top = 62
    Width = 121
    Height = 25
    Caption = 'Open'
    TabOrder = 2
    OnClick = btOpenClick
  end
end
