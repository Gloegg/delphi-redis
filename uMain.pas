unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TfrmMain = class(TForm)
    edHost: TEdit;
    edPort: TEdit;
    btOpen: TButton;
    Label1: TLabel;
    Label2: TLabel;
    procedure btOpenClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses
  Redis, uTerminalFrm;

procedure TfrmMain.btOpenClick(Sender: TObject);
begin
  Application.CreateForm(TfrmTerminal, frmTerminal);
  frmTerminal.SetConnection(TRedis.getConnection(edHost.Text, StrToInt(edPort.Text)));
  frmTerminal.Show;
end;

end.
