object frmTerminal: TfrmTerminal
  Left = 0
  Top = 0
  Caption = 'Terminal'
  ClientHeight = 425
  ClientWidth = 641
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  DesignSize = (
    641
    425)
  PixelsPerInch = 96
  TextHeight = 13
  object meTerminal: TMemo
    Left = 8
    Top = 8
    Width = 625
    Height = 382
    TabStop = False
    Anchors = [akLeft, akTop, akRight, akBottom]
    Color = clBtnFace
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 2
    ExplicitWidth = 619
    ExplicitHeight = 217
  end
  object btSend: TButton
    Left = 558
    Top = 396
    Width = 75
    Height = 21
    Anchors = [akRight, akBottom]
    Caption = 'Send'
    Default = True
    TabOrder = 1
    OnClick = btSendClick
    ExplicitLeft = 552
    ExplicitTop = 231
  end
  object edInput: TEdit
    Left = 8
    Top = 396
    Width = 544
    Height = 21
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 0
    ExplicitTop = 231
    ExplicitWidth = 538
  end
end
